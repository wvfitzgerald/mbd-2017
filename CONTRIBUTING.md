Thanks for contributing to `mbd_2017` (Underscores) — you rock!

## Maintainers

`mbd_2017` is maintained by the [Automattic Theme Team](https://themeshaper.com/about/).

## Submitting issues

Before submitting your issue, make sure it has not been discussed earlier. You can search for existing tickets [here](https://github.com/Automattic/mbd_2017/search).

Here are some tips to consider and to help you write a great report:

* `mbd_2017` supports Microsoft Internet Explorer 11 and Edge, as well as the latest two versions of all other major browsers.
* `mbd_2017` is backwards compatible with the two versions prior to the current stable version of WordPress.
* `mbd_2017` uses HTML5 markup.
* We decided not to include translations [[#50](https://github.com/Automattic/mbd_2017/pull/50)] beyond the existing `mbd_2017.pot` file, a RTL stylesheet [[#263](https://github.com/Automattic/mbd_2017/pull/263)], or editor styles [[#225](https://github.com/Automattic/mbd_2017/pull/225)], as they are likely to change during development of an `mbd_2017` based theme.

## Contributing code

Found a bug you can fix? Fantastic! Patches are always welcome. Here's a few tips for crafting a great pull request:

* Include the purpose of your PR. Be explicit about the issue your PR solves.
* Reference any existing issues that relate to your PR. This allows everyone to easily see all related discussions.
* When submitting a change that affects CSS, please make sure that both SCSS sources and output CSS have been updated equally.

By contributing code to `mbd_2017`, you grant its use under the [GNU General Public License v2 (or later)](LICENSE).

## Underscores.me

If your issue is specific to the [Underscores.me](http://underscores.me) website, the [Underscores.me GitHub repo](https://github.com/Automattic/underscores.me) is the right place for you.

The preferred method of generating a new theme based on `mbd_2017` is the [Underscores.me](http://underscores.me) website. If you have an alternative method, such as a shell script, write a blog post about it or host it in a separate repo -- and make sure to mention [@underscoresme](https://twitter.com/underscoresme) in your tweets!

Want to have your avatar listed as one of the `mbd_2017` contributors [here](http://underscores.me/#contribute)? Just make sure you have an email address added to both GitHub and your local Git installation.