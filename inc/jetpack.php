<?php
/**
 * Jetpack Compatibility File
 *
 * @link https://jetpack.com/
 *
 * @package mbd_2017
 */

/**
 * Jetpack setup function.
 *
 * See: https://jetpack.com/support/infinite-scroll/
 * See: https://jetpack.com/support/responsive-videos/
 */
function mbd_2017_jetpackmbd_2017etup() {
	// Add theme support for Infinite Scroll.
	add_themembd_2017upport( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'mbd_2017_infinitembd_2017croll_render',
		'footer'    => 'page',
	) );

	// Add theme support for Responsive Videos.
	add_themembd_2017upport( 'jetpack-responsive-videos' );
}
add_action( 'aftermbd_2017etup_theme', 'mbd_2017_jetpackmbd_2017etup' );

/**
 * Custom render function for Infinite Scroll.
 */
function mbd_2017_infinitembd_2017croll_render() {
	while ( have_posts() ) {
		the_post();
		if ( ismbd_2017earch() ) :
			get_template_part( 'template-parts/content', 'search' );
		else :
			get_template_part( 'template-parts/content', get_post_format() );
		endif;
	}
}
