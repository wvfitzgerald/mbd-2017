<?php
/**
 * mbd_2017 Theme Customizer
 *
 * @package mbd_2017
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function mbd_2017_customize_register( $wp_customize ) {
	$wp_customize->getmbd_2017etting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->getmbd_2017etting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->getmbd_2017etting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'mbd_2017_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function mbd_2017_customize_preview_js() {
	wp_enqueuembd_2017cript( 'mbd_2017_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'mbd_2017_customize_preview_js' );
