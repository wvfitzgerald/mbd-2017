# Copyright (C) 2016 Automattic
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: mbd_2017 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/tags/mbd_2017\n"
"POT-Creation-Date: 2016-12-23 16:00+0100\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-12-23 16:00+0100\n"
"Last-Translator: \n"
"Language-Team: \n"

#: 404.php:17
#@ mbd_2017
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:21
#@ mbd_2017
msgid "It looks like nothing was found at this location. Maybe try one of the links below or a search?"
msgstr ""

#: 404.php:33
#@ mbd_2017
msgid "Most Used Categories"
msgstr ""

#. translators: %1$s: smiley
#: 404.php:51
#, php-format
#@ mbd_2017
msgid "Try looking in the monthly archives. %1$s"
msgstr ""

#. translators: 1: title.
#: comments.php:34
#, php-format
#@ mbd_2017
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr ""

#. translators: 1: comment count number, 2: title.
#: comments.php:40
#, php-format
#@ mbd_2017
msgctxt "comments title"
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:50 comments.php:71
#@ mbd_2017
msgid "Comment navigation"
msgstr ""

#: comments.php:53 comments.php:74
#@ mbd_2017
msgid "Older Comments"
msgstr ""

#: comments.php:54 comments.php:75
#@ mbd_2017
msgid "Newer Comments"
msgstr ""

#: comments.php:88
#@ mbd_2017
msgid "Comments are closed."
msgstr ""

#: footer.php:18
#@ mbd_2017
msgid "https://wordpress.org/"
msgstr ""

#: footer.php:18
#, php-format
#@ mbd_2017
msgid "Proudly powered by %s"
msgstr ""

#: footer.php:20
#, php-format
#@ mbd_2017
msgid "Theme: %1$s by %2$s."
msgstr ""

#: functions.php:47
#@ mbd_2017
msgid "Primary"
msgstr ""

#: functions.php:93
#@ mbd_2017
msgid "Sidebar"
msgstr ""

#: functions.php:95
#@ mbd_2017
msgid "Add widgets here."
msgstr ""

#: header.php:24
#@ mbd_2017
msgid "Skip to content"
msgstr ""

#: header.php:44
#@ mbd_2017
msgid "Primary Menu"
msgstr ""

#: inc/template-tags.php:28
#, php-format
#@ mbd_2017
msgctxt "post date"
msgid "Posted on %s"
msgstr ""

#: inc/template-tags.php:33
#, php-format
#@ mbd_2017
msgctxt "post author"
msgid "by %s"
msgstr ""

#. translators: used between list items, there is a space after the comma
#: inc/template-tags.php:50 inc/template-tags.php:56
#@ mbd_2017
msgid ", "
msgstr ""

#: inc/template-tags.php:52
#, php-format
#@ mbd_2017
msgid "Posted in %1$s"
msgstr ""

#: inc/template-tags.php:58
#, php-format
#@ mbd_2017
msgid "Tagged %1$s"
msgstr ""

#. translators: %s: post title
#: inc/template-tags.php:65
#, php-format
#@ mbd_2017
msgid "Leave a Comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#. translators: %s: Name of current post
#: inc/template-tags.php:72 template-parts/content-page.php:34
#, php-format
#@ mbd_2017
msgid "Edit %s"
msgstr ""

#: search.php:19
#, php-format
#@ mbd_2017
msgid "Search Results for: %s"
msgstr ""

#: template-parts/content-none.php:14
#@ mbd_2017
msgid "Nothing Found"
msgstr ""

#: template-parts/content-none.php:21
#, php-format
#@ mbd_2017
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:25
#@ mbd_2017
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr ""

#: template-parts/content-none.php:31
#@ mbd_2017
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr ""

#: template-parts/content-page.php:22 template-parts/content.php:38
#@ mbd_2017
msgid "Pages:"
msgstr ""

#. translators: %s: Name of current post.
#: template-parts/content.php:33
#, php-format
#@ mbd_2017
msgid "Continue reading %s <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#. Theme Name of the plugin/theme
#@ mbd_2017
msgid "mbd_2017"
msgstr ""

#. Theme URI of the plugin/theme
#@ mbd_2017
msgid "http://underscores.me/"
msgstr ""

#. Description of the plugin/theme
#@ mbd_2017
msgid "Hi. I'm a starter theme called <code>mbd_2017</code>, or <em>underscores</em>, if you like. I'm a theme meant for hacking so don't use me as a <em>Parent Theme</em>. Instead try turning me into the next, most awesome, WordPress theme out there. That's what I'm here for."
msgstr ""

#. Author of the plugin/theme
#@ mbd_2017
msgid "Automattic"
msgstr ""

#. Author URI of the plugin/theme
#@ mbd_2017
msgid "http://automattic.com/"
msgstr ""
