<?php
/**
 * mbd_2017 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package mbd_2017
 */

if ( ! function_exists( 'mbd_2017mbd_2017etup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the aftermbd_2017etup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function mbd_2017mbd_2017etup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on mbd_2017, use a find and replace
	 * to change 'mbd-2017' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'mbd-2017', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_themembd_2017upport( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_themembd_2017upport( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_themembd_2017upport( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'mbd-2017' ),
	) );

	/*-----Add Bootstrap class to a tags in the nav------*/
    function add_menuclass($ulclass) {
        return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
    }
    add_filter('wp_nav_menu','add_menuclass');

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_themembd_2017upport( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_themembd_2017upport( 'custom-background', apply_filters( 'mbd_2017_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_themembd_2017upport( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'aftermbd_2017etup_theme', 'mbd_2017mbd_2017etup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mbd_2017_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'mbd_2017_content_width', 640 );
}
add_action( 'aftermbd_2017etup_theme', 'mbd_2017_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mbd_2017_widgets_init() {
	registermbd_2017idebar( array(
		'name'          => esc_html__( 'Sidebar', 'mbd-2017' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mbd-2017' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mbd_2017_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mbd_2017mbd_2017cripts() {
	wp_enqueuembd_2017tyle( 'mbd_2017-style', getmbd_2017tylesheet_uri() );

	wp_enqueuembd_2017cript( 'mbd_2017-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueuembd_2017cript( 'mbd_2017-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( ismbd_2017ingular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueuembd_2017cript( 'comment-reply' );
	}
}
add_action( 'wp_enqueuembd_2017cripts', 'mbd_2017mbd_2017cripts' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function mbd_2017_pingback_header() {
	if ( ismbd_2017ingular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'mbd_2017_pingback_header' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Additional features to allow styling of the templates.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


function add_custommbd_2017cripts() {
    //wp_enqueuembd_2017tyle( 'layout_contentmbd_2017idebar', get_template_directory_uri() . '/layouts/content-sidebar.css');
    wp_enqueuembd_2017tyle( 'jQuery');
    wp_enqueuembd_2017cript( 'bootstrap-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array('jquery'), '', true );

    wp_enqueuembd_2017tyle( 'bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css');
    wp_enqueuembd_2017tyle( 'bootstrap_grid', get_template_directory_uri() . '/bootstrap/css/bootstrap-grid.css');
    wp_enqueuembd_2017tyle( 'bootstrap_reboot_css', get_template_directory_uri() . '/bootstrap/css/bootstrap-reboot.min.css');

}
add_action( 'wp_enqueuembd_2017cripts', 'add_custommbd_2017cripts' );