<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mbd_2017
 */

if ( ! is_activembd_2017idebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area col-sm-12 col-md-3" role="complementary">
	<?php dynamicmbd_2017idebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
